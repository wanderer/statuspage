import time
from typing import List, Optional
from pydantic import BaseModel, HttpUrl


class ServiceBase(BaseModel):
	name: str
	owner_id: int
	is_private: bool = True
	description: Optional[str] = None
	service_type: int
	url: HttpUrl
	is_active: bool = True

class Service(BaseModel):
	created_unix: float
	updated_unix: float = None

class ServiceCreate(ServiceBase):
	pass

class ServiceUpdate(ServiceBase):
	name: Optional[str]
	owner_id: Optional[int]
	is_private: Optional[bool]
	description: Optional[str]
	service_type: Optional[int]
	url: Optional[HttpUrl]
	is_active: Optional[bool]

class Service(ServiceBase):
	pass

	class Config:
		orm_mode = True



class UserBase(BaseModel):
	name: str
	full_name: Optional[str] = None
	is_active: bool = True
	is_superuser: bool = False

class User(BaseModel):
	created_unix: float
	updated_unix: float = None
	last_login_unix: float = None

class UserCreate(UserBase):
	password: str

class UserUpdate(UserBase):
	name: Optional[str]
	full_name: Optional[str]
	password: Optional[str]
	is_active: Optional[bool]
	is_superuser: Optional[bool]

class User(UserBase):
	pass

	class Config:
		orm_mode = True



class MetricBase(BaseModel):
	service_id: int
	http_response_code: int

class Metric(BaseModel):
	request_unix: float
	response_unix: float

class MetricCreate(UserBase):
	status: int

class Metric(MetricBase):
	pass

	class Config:
		orm_mode = True



class APISimpleSuccessResponse(BaseModel):
	success: bool = True

class APISimpleErrorResponse(BaseModel):
	success: bool = False
	errors: Optional[List[str]]

class Token(BaseModel):
	access_token: str
	token_type: str

class TokenPayload(BaseModel):
	sub: str = None

