from __future__ import absolute_import
from celery import Celery

app = Celery('app',
	broker='amqp://cl:cl1234@localhost/cl_vhost',
	backend='rpc://',
	include=['app.tasks'])
