from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Float
from sqlalchemy.orm import relationship

from app.database import Base

class Service(Base):
	__tablename__ = "service"

	id = Column(Integer, primary_key=True, index=True)
	name = Column(String, index=True)
	owner_id = Column(Integer, ForeignKey("user.id"), index=True)
	is_private = Column(Boolean, default=True)
	description = Column(String, default=None)
	service_type = Column(Integer)
	url = Column(String)
	is_active = Column(Boolean, default=True, index=True)
	created_unix = Column(Float)
	updated_unix = Column(Float, default=None)

	user = relationship("User", back_populates="service")


class User(Base):
	__tablename__ = "user"

	id = Column(Integer, primary_key=True, index=True)
	name = Column(String, unique=True, index=True)
	full_name = Column(String, index=True)
	password = Column(String)
	is_active = Column(Boolean, index=True)
	is_superuser = Column(Boolean, default=False)
	created_unix = Column(Float)
	updated_unix = Column(Float, default=None)
	last_login_unix = Column(Float, default=None)

	service = relationship("Service", back_populates="user")


class Metric(Base):
	__tablename__ = "metric"

	id = Column(Integer, primary_key=True, index=True)
	service_id = Column(Integer, ForeignKey("service.id"), index=True)
	request_unix = Column(Float)
	response_unix = Column(Float)
	http_response_code = Column(Integer)
	status = Column(Integer)

	#  service = relationship("Service", back_populates="metric")

