import time, asyncio, uvicorn
from celery import Celery
from celery.schedules import crontab
from app.celery import app
from app import ultrametrics
from app.main import app as statuspage

@app.task
def metrics():
	task = asyncio.create_task(ultrametrics.main())

@app.task
def api():
	task = asyncio.create_task(uvicorn.run(statuspage))
