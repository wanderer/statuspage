from pathlib import Path
from typing import Optional
from pydantic import AnyHttpUrl

from starlette.config import Config


p: Path = Path(__file__).parents[2] / "statuspagerc"
config: Config = Config(p if p.exists() else None)

SERVER_NAME: Optional[str]
SERVER_HOST: Optional[AnyHttpUrl]


DATABASE: str = config("DATABASE", cast=str)

ALEMBIC_CONFIG: str = (
	DATABASE
)

JWT_SECRET: str = config("JWT_SECRET", cast=str)
JWT_ALGORITHM: str = config("JWT_ALGORITHM", cast=str)
JWT_EXPIRY: int = config("JWT_EXPIRY", cast=int)

